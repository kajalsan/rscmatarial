﻿using Abp.Authorization;
using exampledemo.Authorization.Roles;
using exampledemo.Authorization.Users;

namespace exampledemo.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
