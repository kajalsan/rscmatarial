﻿namespace exampledemo
{
    public class exampledemoConsts
    {
        public const string LocalizationSourceName = "exampledemo";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
