﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using exampledemo.Configuration;

namespace exampledemo.Web.Host.Startup
{
    [DependsOn(
       typeof(exampledemoWebCoreModule))]
    public class exampledemoWebHostModule: AbpModule
    {
        private readonly IWebHostEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public exampledemoWebHostModule(IWebHostEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(exampledemoWebHostModule).GetAssembly());
        }
    }
}
