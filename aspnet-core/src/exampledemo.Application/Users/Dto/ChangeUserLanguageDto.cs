using System.ComponentModel.DataAnnotations;

namespace exampledemo.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}