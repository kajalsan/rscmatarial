﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using exampledemo.Configuration.Dto;

namespace exampledemo.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : exampledemoAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
