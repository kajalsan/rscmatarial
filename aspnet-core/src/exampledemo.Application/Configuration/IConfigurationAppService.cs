﻿using System.Threading.Tasks;
using exampledemo.Configuration.Dto;

namespace exampledemo.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
