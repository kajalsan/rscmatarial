﻿using System.Threading.Tasks;
using Abp.Application.Services;
using exampledemo.Sessions.Dto;

namespace exampledemo.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
