﻿using System.Threading.Tasks;
using Abp.Application.Services;
using exampledemo.Authorization.Accounts.Dto;

namespace exampledemo.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
