﻿using Abp.Application.Services;
using exampledemo.MultiTenancy.Dto;

namespace exampledemo.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

