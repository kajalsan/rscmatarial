﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using exampledemo.Authorization;

namespace exampledemo
{
    [DependsOn(
        typeof(exampledemoCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class exampledemoApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<exampledemoAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(exampledemoApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
        }
    }
}
