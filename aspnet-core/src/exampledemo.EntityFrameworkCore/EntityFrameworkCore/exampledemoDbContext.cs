﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using exampledemo.Authorization.Roles;
using exampledemo.Authorization.Users;
using exampledemo.MultiTenancy;

namespace exampledemo.EntityFrameworkCore
{
    public class exampledemoDbContext : AbpZeroDbContext<Tenant, Role, User, exampledemoDbContext>
    {
        /* Define a DbSet for each entity of the application */
        
        public exampledemoDbContext(DbContextOptions<exampledemoDbContext> options)
            : base(options)
        {
        }
    }
}
