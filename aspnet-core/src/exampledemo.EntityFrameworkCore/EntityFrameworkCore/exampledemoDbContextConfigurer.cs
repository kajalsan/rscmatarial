using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace exampledemo.EntityFrameworkCore
{
    public static class exampledemoDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<exampledemoDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<exampledemoDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
