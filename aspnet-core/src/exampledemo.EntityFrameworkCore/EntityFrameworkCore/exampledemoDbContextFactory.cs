﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using exampledemo.Configuration;
using exampledemo.Web;

namespace exampledemo.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class exampledemoDbContextFactory : IDesignTimeDbContextFactory<exampledemoDbContext>
    {
        public exampledemoDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<exampledemoDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            exampledemoDbContextConfigurer.Configure(builder, configuration.GetConnectionString(exampledemoConsts.ConnectionStringName));

            return new exampledemoDbContext(builder.Options);
        }
    }
}
