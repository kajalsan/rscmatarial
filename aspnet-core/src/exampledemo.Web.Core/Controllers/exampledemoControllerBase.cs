using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace exampledemo.Controllers
{
    public abstract class exampledemoControllerBase: AbpController
    {
        protected exampledemoControllerBase()
        {
            LocalizationSourceName = exampledemoConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
