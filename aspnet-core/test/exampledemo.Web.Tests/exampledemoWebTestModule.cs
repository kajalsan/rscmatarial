﻿using Abp.AspNetCore;
using Abp.AspNetCore.TestBase;
using Abp.Modules;
using Abp.Reflection.Extensions;
using exampledemo.EntityFrameworkCore;
using exampledemo.Web.Startup;
using Microsoft.AspNetCore.Mvc.ApplicationParts;

namespace exampledemo.Web.Tests
{
    [DependsOn(
        typeof(exampledemoWebMvcModule),
        typeof(AbpAspNetCoreTestBaseModule)
    )]
    public class exampledemoWebTestModule : AbpModule
    {
        public exampledemoWebTestModule(exampledemoEntityFrameworkModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbContextRegistration = true;
        } 
        
        public override void PreInitialize()
        {
            Configuration.UnitOfWork.IsTransactional = false; //EF Core InMemory DB does not support transactions.
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(exampledemoWebTestModule).GetAssembly());
        }
        
        public override void PostInitialize()
        {
            IocManager.Resolve<ApplicationPartManager>()
                .AddApplicationPartsIfNotAddedBefore(typeof(exampledemoWebMvcModule).Assembly);
        }
    }
}