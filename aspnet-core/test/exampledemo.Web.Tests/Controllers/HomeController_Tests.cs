﻿using System.Threading.Tasks;
using exampledemo.Models.TokenAuth;
using exampledemo.Web.Controllers;
using Shouldly;
using Xunit;

namespace exampledemo.Web.Tests.Controllers
{
    public class HomeController_Tests: exampledemoWebTestBase
    {
        [Fact]
        public async Task Index_Test()
        {
            await AuthenticateAsync(null, new AuthenticateModel
            {
                UserNameOrEmailAddress = "admin",
                Password = "123qwe"
            });

            //Act
            var response = await GetResponseAsStringAsync(
                GetUrl<HomeController>(nameof(HomeController.Index))
            );

            //Assert
            response.ShouldNotBeNullOrEmpty();
        }
    }
}