import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class CommonService {

    // private invokeEvent = new Subject <any>();

    private invokeDocument = new Subject<boolean>();
    private invokeLocation = new Subject<boolean>();
    private invokeContact = new Subject<boolean>();


    private invokeLocationForEdit = new Subject<boolean>();
    private invokeAddLoadPickupLocationComponent = new Subject<number>();
    private invokeAddLoadDropoffLocationComponent = new Subject<number>();
    private editLocation = new Subject<number>();
    private editContact = new Subject<number>();
    private editPick = new Subject<boolean>();
    private editDrop = new Subject<boolean>();
    private editTrackPick = new Subject<boolean>();
    private editTrackDrop = new Subject<boolean>();

    locationListEventedit = this.invokeLocationForEdit.asObservable();
    AddLoadPickupLocationComponent = this.invokeAddLoadPickupLocationComponent.asObservable();
    AddLoadDropoffLocationComponent = this.invokeAddLoadDropoffLocationComponent.asObservable();
    documentListEvent = this.invokeDocument.asObservable();
    locationListEvent = this.invokeLocation.asObservable();
    contactInfoListEvent = this.invokeContact.asObservable();

    editLocationEvent = this.editLocation.asObservable();
    editContactEvent = this.editContact.asObservable();
    editPickEvent = this.editPick.asObservable();
    editDropEvent = this.editDrop.asObservable();

    editTrackPickEvent = this.editTrackPick.asObservable();
    editTrackDropEvent = this.editTrackDrop.asObservable();

    private newLocationdEvent = new Subject<number>();
    private AddloadIdEvent = new Subject<boolean>();
    private AddloadChecklist = new Subject<boolean>();
    private carrierInsurance = new Subject<boolean>();
    private carrierDocument = new Subject<boolean>();
    private carrierLocation = new Subject<boolean>();
    private carrierContact = new Subject<boolean>();
    private carrierEquipment = new Subject<boolean>();
    private carrierPrefferedLanes = new Subject<boolean>();
    private carrierBasic = new Subject<boolean>();

    private editCarrierLocation = new Subject<number>();
    private editCarrierContact = new Subject<number>();
    private editCarrierEquipment = new Subject<number>();
    private editCarrierPrefferedLanes = new Subject<number>();
    private editCarrierInsurance = new Subject<number>();
    private editCarrierDocuments = new Subject<number>();

    private editCarrierInfo = new Subject<any>();

    private commonTypeForm = new Subject<boolean>();
    private editCommonTypeForm = new Subject<number>();
    invokeEvent: Subject<any> = new Subject();

    // invokedEvent = this.invokeEvent.asObservable();

    remotecheckinAddloadIdEvent = this.AddloadIdEvent.asObservable();
    NewLocationComponentIdEvent = this.newLocationdEvent.asObservable();

    addloadchecklistEvents = this.AddloadChecklist.asObservable();
    carrierInsuranceEvent = this.carrierInsurance.asObservable();
    carrierDocumentEvents = this.carrierDocument.asObservable();
    carrierLocationEvent = this.carrierLocation.asObservable();
    carrierContactEvent = this.carrierContact.asObservable();
    carrierEquipmentEvent = this.carrierEquipment.asObservable();
    carrierPrefferedLanesEvent = this.carrierPrefferedLanes.asObservable();
    carrierBasicEvent = this.carrierBasic.asObservable();

    editCarrierLocationEvent = this.editCarrierLocation.asObservable();
    editCarrierContactEvent = this.editCarrierContact.asObservable();
    editCarrierEquipmentEvent = this.editCarrierEquipment.asObservable();
    editCarrierPrefferedLanesEvent = this.editCarrierPrefferedLanes.asObservable();
    editCarrierInsuranceEvent = this.editCarrierInsurance.asObservable();
    editCarrierDocumentsEvent = this.editCarrierDocuments.asObservable();

    editCarrierInfoEvent = this.editCarrierInfo.asObservable();

    commonTypeFormEvent = this.commonTypeForm.asObservable();
    editCommonTypeFormEvent = this.editCommonTypeForm.asObservable();

    private invokeConfirmedLoadData = new Subject<boolean>();
    private invokeLoadData = new Subject<boolean>();
    private invokeEDILoadData = new Subject<boolean>();
    private invokeCopyLoadData = new Subject<number>();
    private invokeAddLoadListData = new Subject<boolean>();
    private invokeTrackAddLoadListData = new Subject<boolean>();
    private invokeUserListData = new Subject<boolean>();
    private invokeRoleListData = new Subject<boolean>();
    private invokeTenantListData = new Subject<boolean>();
    private invokeShowBidAutoRefrsh = new Subject<boolean>();
    private invokeInsuranceAutoRefresh = new Subject<boolean>();
    private invokeDocumentsAutoRefresh = new Subject<boolean>();
    private invokeTrackingDetailsComponent = new Subject<any>();
    private invokeDriverDetailsComponent = new Subject<boolean>();
    private invokeEquipmentAutoRefresh = new Subject<boolean>();
    private invokePrefferedLanesAutoRefresh = new Subject<boolean>();
    private invokeInsuranceComponent = new Subject<boolean>();
    private invokeTrackingPDEditTimeComponent = new Subject<boolean>();
    private invokeCarrierDetailsCreateLoad = new Subject<number>();
    private invokeBacktoBidBord = new Subject<number>();
    private invokeApplyFilterFromConfirmedBids = new Subject<any>();
    private invokeCancelButtonHide = new Subject<boolean>();
    private invokePreloadRefresh = new Subject<number>();
    private invokeCreateCancle = new Subject<boolean>();
    private invokeCustomerRefresh = new Subject<number>();
    private invokeContactAutoRefresh = new Subject<boolean>();
    private invokeBasicAutoRefresh = new Subject<boolean>();
    private invokeCustomerEdit = new Subject<number>();
    private invokeAddLoadCustomer = new Subject<number>();
    private invokeAddLoadPickup = new Subject<boolean>();
    private invokeCustomerHeading = new Subject<any>();
    private invokeAllBidCount = new Subject<number>();
    private invokeConfirmedCount = new Subject<number>();
    private invokeDocumnetTab = new Subject<boolean>();
    private invokeLoadEditTab = new Subject<boolean>();
    private invokeMap = new Subject<boolean>();
    private invokeAllEdiCount = new Subject<number>();
    private invokeInvoice = new Subject<boolean>();
    private invokeDispute = new Subject<boolean>();
    private invokeReconcile = new Subject<boolean>();
    private invokeTrack=new Subject<boolean>();
    private invokeDeliver=new Subject<boolean>();
    private invokeCancel=new Subject<boolean>();



    loadDataEvent = this.invokeLoadData.asObservable();
    ConfirmedloadDataEvent = this.invokeConfirmedLoadData.asObservable();
    EDIloadDataEvent = this.invokeEDILoadData.asObservable();
    copyLoadDataEvent = this.invokeCopyLoadData.asObservable();
    addLoadListDataEvent = this.invokeAddLoadListData.asObservable();
    trackAddLoadListDataEvent = this.invokeTrackAddLoadListData.asObservable();
    userListDataEvent = this.invokeUserListData.asObservable();
    roleListDataEvent = this.invokeRoleListData.asObservable();
    tenantListDataEvent = this.invokeTenantListData.asObservable();
    showBidAutoRefrsh = this.invokeShowBidAutoRefrsh.asObservable();
    insuranceAutoRefresh = this.invokeInsuranceAutoRefresh.asObservable();
    DocumentsAutoRefresh = this.invokeDocumentsAutoRefresh.asObservable();
    trackingDetails = this.invokeTrackingDetailsComponent.asObservable();
    driverDetails = this.invokeDriverDetailsComponent.asObservable();
    equipmentAutoRefresh = this.invokeEquipmentAutoRefresh.asObservable();
    prefferedLanesAutoRefresh = this.invokePrefferedLanesAutoRefresh.asObservable();
    insuranceDataEvent = this.invokeInsuranceComponent.asObservable();
    trackingPDEditTime = this.invokeTrackingPDEditTimeComponent.asObservable();
    carrierDetailsCreateLoad = this.invokeCarrierDetailsCreateLoad.asObservable();
    preLoadCarrierDetails = this.invokeCarrierDetailsCreateLoad.asObservable();
    backtoBidBord = this.invokeBacktoBidBord.asObservable();
    applyFilterFromConfirmedBids = this.invokeApplyFilterFromConfirmedBids.asObservable();
    cancelButtonHide = this.invokeCancelButtonHide.asObservable();
    preloadRefresh = this.invokePreloadRefresh.asObservable();
    createCancle = this.invokeCreateCancle.asObservable();
    customerRefresh = this.invokeCustomerRefresh.asObservable();
    contactAutoRefresh = this.invokeContactAutoRefresh.asObservable();
    basicAutoRefresh = this.invokeBasicAutoRefresh.asObservable();
    customerEdit = this.invokeCustomerEdit.asObservable();
    addLoadCustomer = this.invokeAddLoadCustomer.asObservable();
    addLoadPickup = this.invokeAddLoadPickup.asObservable();
    customerHeading = this.invokeCustomerHeading.asObservable();
    allBidCount = this.invokeAllBidCount.asObservable();
    confirmedCount = this.invokeConfirmedCount.asObservable();
    DocumentTabroute = this.invokeDocumnetTab.asObservable();
    EditLoadTab = this.invokeLoadEditTab.asObservable();
    MapGoTo = this.invokeMap.asObservable();
    allEdiCount = this.invokeAllEdiCount.asObservable();
    Invoice = this.invokeInvoice.asObservable();
    Dispute = this.invokeDispute.asObservable();
    Reconcile = this.invokeReconcile.asObservable();
    Tracking =this.invokeTrack.asObservable();
    Delivered = this.invokeDeliver.asObservable();
    Cancelled = this.invokeCancel.asObservable();




    constructor() { }

    // callMethodOfSecondComponent(number) {
    //     this.invokeEvent.next(number);
    // }

    callMethodOfSecondComponent(someValue) {
        this.invokeEvent.next(someValue)
    }

    callMethodOfUserComponent(number) {
        this.invokeUserListData.next(number);
    }
    callMethodOfRoleComponent(number) {
        this.invokeRoleListData.next(number);
    }
    callMethodOfTenantComponent(number) {
        this.invokeTenantListData.next(number);
    }

    callMethodOfAddLoadComponent(number) {
        this.invokeLoadData.next(number);
    }

    callMethodOfConfirmedLoadComponent(number) {
        this.invokeConfirmedLoadData.next(number);
    }

    callMethodOfEDILoadComponent(number) {
        this.invokeEDILoadData.next(number);
    }

    callMethodOfCopyLoadData(number) {
        this.invokeCopyLoadData.next(number);
    }

    callMethodOfAddLoadListData(boolean) {
        this.invokeAddLoadListData.next(boolean);
    }

    callMethodInsuranceComponent(boolean) {
        this.invokeInsuranceComponent.next(boolean);
    }
    callMethodOfTrackAddLoadListData(boolean) {
        this.invokeTrackAddLoadListData.next(boolean);
    }

    callMethodOfDocumentComponent(number) {
        this.invokeDocument.next(number);
    }

    callMethodOfLocationComponent(number) {
        this.invokeLocation.next(number);
    }

    callMethodOfLocationComponentEdit(number) {
        this.invokeLocationForEdit.next(number);
    }

    callMethodOfAddLoadPickupLocationComponent(number) {
        this.invokeAddLoadPickupLocationComponent.next(number);
    }

    callMethodOfAddLoadDropOffLocationComponent(number) {
        this.invokeAddLoadDropoffLocationComponent.next(number);
    }

    callMethodOfContactInfoComponent(number) {
        this.invokeContact.next(number);
    }

    editLocationComponent(id) {
        this.editLocation.next(id);
    }

    editContactInfoComponent(id) {
        this.editContact.next(id);
    }

    editPickupComponent(boolean) {
        this.editPick.next(boolean);
    }

    editDropoffComponent(boolean) {
        this.editDrop.next(boolean);
    }

    editTrackPickupComponent(boolean) {
        this.editTrackPick.next(boolean);
    }

    editTrackDropoffComponent(boolean) {
        this.editTrackDrop.next(boolean);
    }

    callMethodOfCarrierInsuranceComponent(number) {
        this.carrierInsurance.next(number);
    }


    callMethodOfCarrierDocumentComponent(number) {
        this.carrierDocument.next(number);
    }

    callMethodOfAddloadChecklistComponent(number) {
        this.AddloadChecklist.next(number);
    }

    callMethodOfCarrierLocationComponent(number) {
        this.carrierLocation.next(number);
    }
    editCarrierLocationComponent(number) {
        this.editCarrierLocation.next(number);
    }


    callMethodOfCarrierContactComponent(number) {
        this.carrierContact.next(number);
    }
    editCarrierContactComponent(number) {
        this.editCarrierContact.next(number);
    }


    callMethodOfCarrierEquipmentComponent(number) {
        this.carrierEquipment.next(number);
    }

    callMethodOfCarrierBasicComponent(number) {
        this.carrierBasic.next(number);
    }
    editCarrierEquipmentComponent(number) {
        this.editCarrierEquipment.next(number);
    }


    callMethodOfCarrierPrefferedLanesComponent(number) {
        this.carrierPrefferedLanes.next(number);
    }
    editCarrierPrefferedLanesComponent(number) {
        this.editCarrierPrefferedLanes.next(number);
    }

    editCarrierInsuranceComponent(number) {
        this.editCarrierInsurance.next(number);
    }

    editCarrierDocumentComponent(number) {
        this.editCarrierDocuments.next(number);
    }

    callMethodOfCarrierInfoComponent(any) {
        this.editCarrierInfo.next(any);
    }

    callMethodOfCommonTypeFormComponent(number) {
        this.commonTypeForm.next(number);
    }
    editCommonTypeFormComponent(number) {
        this.editCommonTypeForm.next(number);
    }

    callMethodOfremotecheckintComponent(number) {
        this.AddloadIdEvent.next(number);
    }

    callMethodOfnewLocationComponent(number) {
        this.newLocationdEvent.next(number);
    }

    callMethodForShowBidAutoRefresh(number) {
        this.invokeShowBidAutoRefrsh.next(number);
    }

    callMethodForInsuranceAutoRefresh(number) {
        this.invokeInsuranceAutoRefresh.next(number);
    }

    callMethodForDocumentsAutoRefresh(number) {
        this.invokeDocumentsAutoRefresh.next(number);
    }

    callMethodForEquipmentAutoRefresh(number) {
        this.invokeEquipmentAutoRefresh.next(number);
    }

    callMethodForBasicAutoRefresh(number) {
        this.invokeBasicAutoRefresh.next(number);
    }
    callMethodForContactAutoRefresh(number) {
        this.invokeContactAutoRefresh.next(number);
    }

    callMethodForPrefferedLanesAutoRefresh(number) {
        this.invokePrefferedLanesAutoRefresh.next(number);
    }

    callMethodForDriverRefresh(boolean) {
        this.invokeDriverDetailsComponent.next(boolean);
    }

    callMethodForTrackingDetailsComponent(bool) {
        this.invokeTrackingDetailsComponent.next(bool);
    }

    callMethodForTrackingPDEditTimeComponent(bool) {
        this.invokeTrackingPDEditTimeComponent.next(bool);
    }

    callMethodForCarrierDetailsCreateLoad(number) {
        this.invokeCarrierDetailsCreateLoad.next(number);
    }

    callMethodForBacktoBidBord(number) {
        this.invokeBacktoBidBord.next(number);
    }

    callMethodForApplyFilterFromConfirmedBids(number) {
        this.invokeApplyFilterFromConfirmedBids.next(number);
    }

    callMethodForCancelButtonHide(bool) {
        this.invokeCancelButtonHide.next(bool);
    }

    callMethodForPreloadRefresh(bool) {
        this.invokePreloadRefresh.next(bool);
    }

    callMethodForCreateCancle(bool) {
        this.invokeCreateCancle.next(bool);
    }

    callMethodForCustomerRefresh(number) {
        this.invokeCustomerRefresh.next(number);
    }

    callMethodForCustomerEdit(number) {
        this.invokeCustomerEdit.next(number);
    }

    callMethodForAddLoadCustomer(number) {
        this.invokeAddLoadCustomer.next(number);
    }

    callMethodForAddLoadPickup(number) {
        this.invokeAddLoadPickup.next(number);
    }

    callMethodForCustomerHeading(any) {
        this.invokeCustomerHeading.next(any);
    }

    callmethodforAllBidCount(number) {
        this.invokeAllBidCount.next(number);
    }

    callmethodforAllEdiCount(number) {
        this.invokeAllEdiCount.next(number);
    }

    callmethodforConfirmedCount(number) {
        this.invokeConfirmedCount.next(number);
    }

    callmethodforDocumnetTab(bool) {
        this.invokeDocumnetTab.next(bool);
    }

    callmethodforLoadeditTab(bool) {
        this.invokeLoadEditTab.next(bool);
    }

    callmethodforMap(bool) {
        this.invokeMap.next(bool);
    }

    callmethodInvoiceAndBill(bool) {
        this.invokeInvoice.next(bool);
    }
    callmethodforDispute(bool) {
        this.invokeDispute.next(bool);
    }
    callmethodforReconcile(bool) {
        this.invokeReconcile.next(bool);
    }
    callmethodforTracking(bool){
        this.invokeTrack.next(bool);
    }
    callmethodforCancelled(bool){
        this.invokeCancel.next(bool);
    }
    callmethodforDelivered(bool){
        this.invokeDeliver.next(bool);
    }
}
