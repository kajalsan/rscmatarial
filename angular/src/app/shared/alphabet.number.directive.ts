import { NgControl } from '@angular/forms';
import { Directive, HostListener, Input } from '@angular/core';
@Directive({
  selector: '[AlphabetNumber]'
})
export class AlphabetNumberDirective {

  
  constructor(public ngControl: NgControl) { }
  // regexStr = '^[a-zA-Z0-9_]*$';
  // @Input() isAlphaNumeric: boolean;

  // constructor(private el: ElementRef) { }

  @HostListener('ngModelChange', ['$event'])
  onModelChange(event,str) {
    this.onInputChange(event,str);
  }

  // @HostListener('keypress', ['$event']) onKeyPress(event) {
  //   return new RegExp(this.regexStr).test(event.key);
  // }

  onInputChange(event,str) {
    let newVal = event.replace(/^[0]/g, '');
    // 
    if (newVal.length === 0) {
      newVal = '';
    } else if (newVal.length <= 20) {
      newVal = newVal.replace(/(^[a-zA-Z1-9])|^(\d{0,20})/,'$1');
    } 
    else {
      newVal = newVal.substring(0, 20);
      newVal = newVal.replace(/(^[a-zA-Z1-9])|^(\d{0,20})/,'$1');
    }
    this.ngControl.valueAccessor.writeValue(newVal);
  // @HostListener('paste', ['$event']) blockPaste(event: KeyboardEvent) {
  //   this.validateFields(event);
  }

  // validateFields(event) {
  //   setTimeout(() => {

  //     this.el.nativeElement.value = this.el.nativeElement.value.replace(/[^A-Za-z ]/g, '').replace(/\s/g, '');
  //     event.preventDefault();

  //   }, 100)
  // }

}



// import { Directive, HostListener, ElementRef, Input } from '@angular/core';


// @Directive({
//   selector: '[AlphabetNumber]'
// })
// export class AlphabetNumberDirective {

//   regexStr = '^[a-zA-Z0-9_]*$';
//   @Input() isAlphaNumeric: boolean;

//   constructor(private el: ElementRef) { }


//   @HostListener('keypress', ['$event']) onKeyPress(event) {
//     return new RegExp(this.regexStr).test(event.key);
//   }

//   @HostListener('paste', ['$event']) blockPaste(event: KeyboardEvent) {
//     this.validateFields(event);
//   }

//   validateFields(event) {
//     setTimeout(() => {

//       this.el.nativeElement.value = this.el.nativeElement.value.replace(/[^A-Za-z ]/g, '').replace(/\s/g, '');
//       event.preventDefault();

//     }, 100)
//   }

// }