import { Directive, HostListener } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[appTimeMask]',
})
export class TimeMaskDirective {

  constructor(public ngControl: NgControl) { }

  @HostListener('ngModelChange', ['$event'])
  onModelChange(event) {
    this.onInputChange(event, false);
  }

  @HostListener('keydown.backspace', ['$event'])
  keydownBackspace(event) {
    //this.onInputChange(event.target.value, true);
  }


  onInputChange(event, backspace) {
    let newVal = event.replace(/\D/g, '');
    if (backspace && newVal.length <= 5 ) {
      newVal = newVal.substring(0, newVal.length - 1);
    }
    if (newVal.length === 0) {
      newVal = '';
    } else if (newVal.length <= 2 ) {
      // newVal = newVal.replace(/^(2[0-4]|1[0-9]|[1-9])/, '$1');
      
      if(+newVal > 23) 
        newVal = '' + 23;
      newVal = newVal.replace(/^(\d{0,2})/, '$1');

    }
    else if (newVal.length <= 4) {
      let newVal1 = newVal.substring(2, newVal.length);
      if(+newVal1 > 59) {
        newVal1 = 59;
        newVal =  newVal.substring(0, 2) + '' + newVal1;

      }
      newVal = newVal.replace(/^(\d{0,2})(\d{0,2})/, '$1:$2');
    }
    else{
      newVal = newVal.substring(0, 4);
      newVal = newVal.replace(/^(\d{0,2})(\d{0,2})/, '$1:$2');
    }
    // //console.log(newVal);
    this.ngControl.valueAccessor.writeValue(newVal);
  }
}