import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-my-dialog',
    templateUrl: './myDialog.component.html',
})

export class MyDialogComponent {
    constructor(
        private _myDialogRef : MatDialogRef<MyDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: string
    ){}

    // onCloseConfirm() {
        
    //     this._myDialogRef.close('Confirm');
    //   }
    
    onCloseCancel() {
    this._myDialogRef.close('Close');
    //console.log(this.data);
    }
}