import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
        name: 'dateFormatPipe',
})
export class dateFormatPipe implements PipeTransform {
       
        transform(value: string): any {

                if (!value) {
                        return '';
                }
                var d= new Date(value);
                       var localOffset = d.getTimezoneOffset() * 60000;
                       var localTime = d.getTime() - localOffset;
        
                       d.setTime(localTime);
                       return d;
                }
}

