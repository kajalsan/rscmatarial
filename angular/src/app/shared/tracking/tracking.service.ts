import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

const httpOptions = {
    headers: new HttpHeaders({
     'Content-Type': 'application/json',
     'Authorization': 'Basic c3BhbnNlQDgyOGxvZ2lzdGljcy5jb206ODI4TG9naXN0aWNz',
     'Accept': 'application/json',
     'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, DELETE, PUT',
     'Access-Control-Allow-Origin': '*',
     'Access-Control-Allow-Headers': "X-Requested-With, Content-Type, Origin, Authorization, Accept, Client-Security-Token, Accept-Encoding"
    })

 };

 
 
 
@Injectable()

export class TrackingService{

    constructor(
        private _http: HttpClient,
    ){}

    get() 
    {
        let url_ = "https://test.p-44.com/api/v4/tl/shipments/64462/statuses";
        //"https://test.p-44.com/api/v3/tl/shipments/64380/statuses"
        let loginDetails = "Basic " + btoa("spanse@828logistics.com:828Logistics");
        let options_ : any = {
            async: true,
            crossDomain: true,
            observe: "response",
            withCredentials: true,
           // responseType: "blob",
            dataType: 'jsonp',
            headers: new HttpHeaders({
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": loginDetails,
                "Access-Control-Allow-Methods": 'POST, GET, OPTIONS, DELETE, PUT',
                "Access-Control-Allow-Origin": '*',
                
                "Access-Control-Allow-Headers": "Content-Type, Authorization"
            })
        };

       return this._http.request("GET", url_, options_).subscribe((response_ : any) => {
                return response_;
            })

    }

    post(){
        let url_ = "https://test.p-44.com/api/v3/tl/shipments";
        const data = '{ "carrierIdentifier": { "type": "SCAC", "value": "ACME1" }, "shipmentIdentifiers": [ { "type": "BILL_OF_LADING", "value": "9789789786890" } ], "shipmentStops": [ { "stopNumber": 1, "appointmentWindow": { "startDateTime": "2018-10-22T09:00:00", "endDateTime": "2018-10-22T17:12:54" }, "location": { "address": { "postalCode": "60603", "addressLines": [ "105 West Adams St" ], "city": "Chicago", "state": "IL", "country": "US" }, "contact": { "companyName": "project44", "contactName": "Emily Pagano", "phoneNumber": "8476916336", "phoneNumber2": "", "email": "epagano@p-44.com", "faxNumber": "" } } }, { "stopNumber": 2, "appointmentWindow": { "startDateTime": "2018-12-14T09:00:00", "endDateTime": "2018-12-14T17:12:54" }, "location": { "address": { "postalCode": "60614", "addressLines": [ "2739 N Magnolia Ave" ], "city": "Chicago", "state": "IL", "country": "US" }, "contact": { "companyName": "Emily Home", "contactName": "Amanda Call", "phoneNumber": "244-555-6666", "phoneNumber2": "", "email": "acall@gmail.com", "faxNumber": "" } } } ], "equipmentIdentifiers": [ { "type": "MOBILE_PHONE_NUMBER", "value": "847-691-6336" } ], "shippingDetails": { "multipleDrivers": false, "truckDetails": { "truckDimensions": { "length": 0, "width": 0, "height": 0, "unitOfMeasure": "IN" }, "weight": 0, "weightUnitOfMeasure": "LB" }, "hazmatDetails": { "hazardClasses": [ "string" ] } }, "apiConfiguration": { "fallBackToDefaultAccountGroup": false } }'; 
        let loginDetails = "Basic " + btoa("spanse@828logistics.com:828Logistics");
        
        let options_ : any = {
            async: true,
            crossDomain: true,
            body: data,
            observe: "response",
           // responseType: "blob",
            dataType: 'jsonp',
            headers: new HttpHeaders({
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": loginDetails,
                "Access-Control-Allow-Methods": 'POST, GET, OPTIONS, DELETE, PUT',
                "Access-Control-Allow-Origin": '*',
                "Access-Control-Allow-Headers": "X-Requested-With, Content-Type, Origin, Authorization, Accept, Client-Security-Token, Accept-Encoding"
            })
        };
            
        return this._http.post(url_, data, options_)
        .subscribe(
            res => {
                //console.log("POST Request is successful ", res);
            },
            error => {
                //console.log("Error", error);
            }
        );

        // return this._http.request("POST", url_, options_).subscribe((response_ : any) => {
        //         return response_;
        //     })

        // let headers= new HttpHeaders({
        //     "Content-Type": 'application/json', 
        //     "Accept": 'application/json',
        //     "Authorization": 'Basic ZXBhZ2Fub0BwLTQ0LmNvbTpFSlBnb256bzNA'
        // })

        // this._http
        //     .post(url_, content_, {
        //         headers: new HttpHeaders({
        //             "Content-Type": 'application/json', 
        //             "Authorization": 'Basic c3BhbnNlQDgyOGxvZ2lzdGljcy5jb206ODI4TG9naXN0aWNz'
        //         })
        //       })
        //     .subscribe(
        //         result => {
        //           //console.log(result);
        //         },
        //         err => {
        //           //console.log("Error- something is wrong!")
        //     });

       // //console.log(data,JSON.parse(data))
        // let url="https://test.p-44.com/api/v3/tl/shipments";
        // return this._http.post(url,data,options_);
        
        // return this._http.post(url,data,httpOptions).map((responce: Response) => {
        //     //console.log(responce.json());
        // })

        

    }

    getByIdWithStatus(){
        let id;
        let url="https://test.p-44.com/api/v3/tl/shipments/"+id+"/statuses";
    }

    getById(){
        let id;
        let url="https://test.p-44.com/api/v4/tl/shipments/"+id;
    }
}