import { Injectable } from '@angular/core';
import swal from 'sweetalert2';
import { load } from '@angular/core/src/render3/instructions';

@Injectable()

export class AlertService {


    showActiveMsg(data: any): any {

        return swal({
            title: "<span style='font-size:17px;'> Confirm Activation?",
            html: "<span style='font-size:15px;'>Would you like to proceed with ACTIVATE all the locations for the selected company?",
            showCancelButton: true,
            confirmButtonText: 'Yes',
            confirmButtonClass: "mat-flat-button mat-primary carrierDeactivationBtn button1",
            cancelButtonText: "No",
            cancelButtonClass: "mat-flat-button mat-accent button1",
            allowOutsideClick: false,
            buttonsStyling: false
        })
            .then((result) => {
                return result;
            })
    }

    showDeactiveMsg(data: any): any {

        return swal({

            title: "<span style='font-size:17px;'>Confirm Deactivation?",
            html: "<span style='font-size:15px;'>This action will DEACTIVATE all the locations for the selected company. This action will also hide selected company while selecting location in Add load process. Would you like to proceed?",

            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: "mat-flat-button mat-primary carrierDeactivationBtn button1",
            cancelButtonClass: "mat-flat-button mat-accent button1",
            buttonsStyling: false,
            allowOutsideClick: false
        })

            .then((result) => {
                return result;
            })
    }





    showCarrierActiveMsg(data: any): any {

        return swal({
            title: "<span style='font-size:17px;'>Confirm Activation?",
            html: "<span style='font-size:15px;'>This action will re-activate this selected carrier. Carrier will receive bid emails and be able to submit bids for consideration. Would you like to proceed?",
            //type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            confirmButtonClass: "mat-flat-button mat-primary carrierDeactivationBtn button1",
            cancelButtonText: "No",
            cancelButtonClass: "mat-flat-button mat-accent button1",
            allowOutsideClick: false,
            buttonsStyling: false
        })

            .then((result) => {
                return result;
            })
    }

    showCarrierDeactiveMsg(data: any): any {

        return swal({

            title: "<span style='font-size:17px;'>Confirm Deactivation",
            html: "<span style='font-size:15px;'>This carrier will stop receiving bid emails and you will no longer be able to be assign them loads, would you like to proceed?",
            showCancelButton: true,
            confirmButtonText: 'Yes ',
            cancelButtonText: 'No',
            confirmButtonClass: "mat-flat-button mat-primary carrierDeactivationBtn button1",
            cancelButtonClass: "mat-flat-button mat-accent button1",
            buttonsStyling: false,
            allowOutsideClick: false
        })

            .then((result) => {
                return result;
            })
    }


    showSwal(data: any): any {
        return swal({
            title: 'Confirm Delete?',
            text: 'Are you sure you would like to Delete this Stop - ' + data,  //this imaginary file!,
            //type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: "mat-flat-button mat-primary carrierDeactivationBtn",
            cancelButtonClass: "mat-flat-button mat-accent",
            buttonsStyling: false,
            allowOutsideClick: false
        })
            .then((result) => {
                return result;
            })
    }


    // showSwal(data: any): any {
    //     return swal({
    //         title: 'Are you sure?',
    //         text: 'You will not be able to recover for ' + data,  //this imaginary file!,
    //         //type: 'warning',
    //         showCancelButton: true,
    //         confirmButtonText: 'Yes',
    //         cancelButtonText: 'No, keep it',
    //         confirmButtonClass: "btn btn-success",
    //         cancelButtonClass: "btn btn-danger",
    //         buttonsStyling: false,
    //         allowOutsideClick: false
    //     })
    //         .then((result) => {
    //             return result;
    //         })
    // }

    showSwalUser(data: any): any {

        return swal({
            // html: '<pre>' + str + '</pre>', 
            title: "<span style='font-size:17px'>Confirm Delete?",
            html: "<span style='font-size:15px'>Are you sure you would like to delete USER" + '<br>' + data,  //this imaginary file!,
            //type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: "mat-flat-button mat-primary margin",
            cancelButtonClass: "mat-flat-button mat-accent",
            buttonsStyling: false,
            width: 400,
            allowOutsideClick: false

        })
            .then((result) => {
                return result;
            })
    }
    showSwalgroup(data: any): any {
        return swal({
            // html: '<pre>' + str + '</pre>', 
            title: "<span style='font-size:17px'>Confirm Delete?",
            html: "<span style='font-size:15px'>Are you sure you would like to delete GROUP" + '<br>' + data,  //this imaginary file!,
            //type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: "mat-flat-button mat-primary margin",
            cancelButtonClass: "mat-flat-button mat-accent",
            buttonsStyling: false,
            width: 400,
            allowOutsideClick: false

        })
            .then((result) => {
                return result;
            })
    }

    showSwalassessorial(data: any): any {
        return swal({
            // html: '<pre>' + str + '</pre>', 
            title: "<span style='font-size:17px'>Confirm Delete?",
            html: "<span style='font-size:15px'>Are you sure you would like to delete Assessorial" + '<br>' + data,  //this imaginary file!,
            //type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: "mat-flat-button mat-primary margin",
            cancelButtonClass: "mat-flat-button mat-accent",
            buttonsStyling: false,
            width: 400,
            allowOutsideClick: false

        })
            .then((result) => {
                return result;
            })
    }


    showLocationSwal(data: any): any {

        return swal({
            title: "<span style='font-size:17px;'>Confirm Delete?",
            html: "<span style='font-size:15px;'>Would you like to DELETE selected LOCATION_CODE - " + data + "?",  //this imaginary file!,
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: "mat-flat-button mat-primary margin",
            cancelButtonClass: "mat-flat-button mat-accent",
            buttonsStyling: false,
            allowOutsideClick: false
        })
            .then((result) => {
                return result;
            })
    }

    showContactSwal(data: any): any {

        return swal({

            title: "<span style='font-size:17px;'>Confirm Delete?",
            html: "<span style='font-size:15px;'>Would you like to DELETE selected CONTACT for this Location?",  //this imaginary file!,
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: "mat-flat-button mat-primary margin",
            cancelButtonClass: "mat-flat-button mat-accent",
            buttonsStyling: false,
            allowOutsideClick: false
        })
            .then((result) => {
                return result;
            })
    }

    showLocationMessage(): any {

        return swal({
            html: "<span style='font-size:17px'>Location Address is Incorrect. Please Select Correct Location.",
            confirmButtonText: 'OK',
            confirmButtonClass: "btn btn-danger",
            buttonsStyling: false,
            width: 400
        })
            .then((result) => {
                return result;
            })
    }

    showLocationTimeMessage(fromTime, toTime, timeZone, locationName): any {
        return swal({
            html: "<span style='font-size:17px'> This window is outside the shipping hours for this location. Shipping Hours for " + locationName + " are " + fromTime + " - " + toTime + " " + timeZone,
            confirmButtonText: 'OK',
            confirmButtonClass: "btn btn-danger",
            buttonsStyling: false,
            width: 400
        })
            .then((result) => {
                return result;
            })
    }

    showSwalAdminEdit(): any {

        return swal({
            // html: '<pre>' + str + '</pre>',
            // title: "<span style='font-size:17px'>Confirm Delete?",
            html: "<span style='font-size:17px'>Admin user can not be modified.",  //this imaginary file!,
            //type: 'warning',
            confirmButtonText: 'OK',

            confirmButtonClass: "btn btn-success",

            buttonsStyling: false,
            width: 400

        })
            .then((result) => {
                return result;
            })
    }

    showDocumentSwal(data: any): any {

        return swal({
            title: "<span style='font-size:17px;'>Confirm Delete?",
            html: "<span style='font-size:15px;'>Would you like to DELETE selected FILE - " + data + "?",  //this imaginary file!,
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: "mat-flat-button mat-primary margin",
            cancelButtonClass: "mat-flat-button mat-accent",
            buttonsStyling: false,
            allowOutsideClick: false
        })
            .then((result) => {
                return result;
            })
    }

    showEquipmentSwal(data: any): any {

        return swal({

            title: "<span style='font-size:17px;'>Confirm Delete?",
            html: "<span style='font-size:15px;'>Are you sure you would like to DELETE selected EQUIPMENT TYPE?",
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: "mat-flat-button mat-primary margin",
            cancelButtonClass: "mat-flat-button mat-accent",
            buttonsStyling: false,
            allowOutsideClick: false
        })
            .then((result) => {
                return result;
            })
    }

    showLanesSwal(data: any): any {

        return swal({

            title: "<span style='font-size:17px;'>Confirm Delete?",
            html: "<span style='font-size:15px;'>Would you like to delete Selected PREFFERED LANE " + data + "?",
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: "mat-flat-button mat-primary margin",
            cancelButtonClass: "mat-flat-button mat-accent",
            buttonsStyling: false,
            allowOutsideClick: false
        })
            .then((result) => {
                return result;
            })
    }

    copyAlert(data: any): any {
        return swal({
            title: 'Are you sure you want to copy this load?',
            text: 'The details of load ' + data + ' will be duplicated.',  //this imaginary file!,
            //type: 'warning',
            showCancelButton: true,
            allowOutsideClick: false,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: "mat-flat-button mat-primary margin",
            cancelButtonClass: "mat-flat-button mat-accent",
            buttonsStyling: false
        })
            .then((result) => {
                return result;
            })
    }

    deleteloadAlert(data: any): any {
        return swal({
            title: 'Are you sure you want to delete this load?',
            showCancelButton: true,
            allowOutsideClick: false,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: "mat-flat-button mat-primary margin",
            cancelButtonClass: "mat-flat-button mat-accent",
            buttonsStyling: false
        })
            .then((result) => {
                return result;
            })
    }

    remoteDocAlert(data, pdtype): any {
        return swal({
            title: "Driver has been Remotely Checked IN/OUT",
            html: data + " is not been uploaded for this " + pdtype + " Location." + "<br>" + "Do You Want to Continue?",  //this imaginary file!,
            //type: 'warning',
            showCancelButton: true,
            allowOutsideClick: false,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: "mat-flat-button mat-primary margin",
            cancelButtonClass: "mat-flat-button mat-accent",
            buttonsStyling: false
        })
            .then((result) => {
                return result;
            })
    }

    acceptAward(data: any) {
        return swal({
            title: 'Thanks for Award Confirmation.',
            text: 'You will receive email along with Confirmation Number that driver can use his phone to enable location tracking for this load #' + data + '.',
            //showCancelButton: true,
            confirmButtonText: 'OK',
            allowOutsideClick: false,
            //  cancelButtonText: 'Cancel',
            confirmButtonClass: "mat-flat-button mat-primary carrierDeativationBtn",
            // cancelButtonClass: "btn btn-danger",
            buttonsStyling: false
        })
            .then((result) => {
                return result;
            })
    }

    acceptCreatAward(data, carrierName): any {
        return swal({
            text: 'Are you sure you want to assign load #' + data + " to " + carrierName,
            showCancelButton: true,
            confirmButtonText: 'OK',
            allowOutsideClick: false,
            cancelButtonText: 'Cancel',
            confirmButtonClass: "mat-flat-button mat-primary carrierDeativationBtn",
            cancelButtonClass: "mat-flat-button mat-accent",
            buttonsStyling: false
        })
            .then((result) => {
                return result;
            })
    }

    EDIerror(result): any {

        return swal({
            title: 'Load Cannot be Created because of?',
            text: result,  //this imaginary file!,
            //type: 'warning',

            allowOutsideClick: false,
            confirmButtonText: 'OK',

            confirmButtonClass: "btn btn-success",

            buttonsStyling: false
        })
            .then((result) => {
                return result;
            })
    }

    P44error(result): any {

        return swal({
            title: 'Load Cannot be confirmed because of?',
            text: result,  //this imaginary file!,
            //type: 'warning',

            allowOutsideClick: false,
            confirmButtonText: 'OK',

            confirmButtonClass: "btn btn-success",

            buttonsStyling: false
        })
            .then((result) => {
                return result;
            })
    }

    //  P44error(result) {
    //     return swal({
    //         title: 'Load Cannot be confirmed because of '+ result,
    //         showConfirmButton: true,
    //         allowOutsideClick: false,
    //         confirmButtonText: 'OK',
    //         confirmButtonClass: "btn btn-warning",
    //         //   buttonsStyling: false
    //     })
    //         .then((result) => {
    //             return result;
    //         })
    // }
    driverAllocate(loadId) {
        return swal({
            title: 'Driver already Allocated for Load #' + loadId,
            showConfirmButton: true,
            allowOutsideClick: false,
            confirmButtonText: 'OK',
            confirmButtonClass: "btn btn-warning",
            //   buttonsStyling: false
        })
            .then((result) => {
                return result;
            })
    }

    timeValidation() {
        return swal({
            title: 'Departure time Must be greater than Arrival time',
            showConfirmButton: true,
            allowOutsideClick: false,
            confirmButtonText: 'OK',
            confirmButtonClass: "btn btn-warning",
            //   buttonsStyling: false
        })
            .then((result) => {
                return result;
            })
    }

    cancelLoad(loadId) {
        return swal({

            text: 'This load has been already Assigned/Awarded to the carrier, Carrier did NOT confirm this load yet. Do you want to permanently delete Load#' + loadId + "?",
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: "mat-flat-button mat-primary carrierDeativationBtn",
            cancelButtonClass: "mat-flat-button mat-accent",
            buttonsStyling: false,
            allowOutsideClick: false
        })
            .then((result) => {
                return result;
            })
    }

    reassignLoad(loadId) {
        return swal({

            text: 'Do you want to Reassign Load # ' + loadId + "?",
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: "mat-flat-button mat-primary carrierDeativationBtn",
            cancelButtonClass: "mat-flat-button mat-accent",
            buttonsStyling: false,
            allowOutsideClick: false
        })
            .then((result) => {
                return result;
            })
    }

    reexportLoad(loadId) {
        return swal({
            title: 'Do you want to Repost Load # ' + loadId + "?",
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: "mat-flat-button mat-primary carrierDeativationBtn",
            cancelButtonClass: "mat-flat-button mat-accent",
            buttonsStyling: false,
            allowOutsideClick: false
        })
            .then((result) => {
                return result;
            })
    }

    dateGreater() {
        return swal({
            title: 'Date/Time Must be Greater than previous Pick-Up/Drop-Off',
            showCloseButton: true,
            allowOutsideClick: false,
            confirmButtonText: 'OK',
            confirmButtonClass: "btn btn-warning",

        })
            .then((result) => {
                return result;
            })
    }

    locationLoad() {
        return swal({
            title: 'Location Not Selected, Please Select Location',
            showCloseButton: true,
            allowOutsideClick: false,
            confirmButtonText: 'OK',
            confirmButtonClass: "btn btn-warning",
        })
            .then((result) => {
                return result;
            })
    }


    loadDelivered() {
        return swal({
            title: 'Load already Delivered',
            showConfirmButton: true,
            allowOutsideClick: false,
            confirmButtonText: 'OK',
            confirmButtonClass: "btn btn-warning",
        })
            .then((result) => {
                return result;
            })
    }

    dateAlert() {
        return swal({
            title: 'Date must be grater than or equal to previous pickup/dropoff',
            showConfirmButton: true,
            allowOutsideClick: false,
            confirmButtonText: 'OK',
            confirmButtonClass: "btn btn-warning",
            //   buttonsStyling: false
        })
            .then((result) => {
                return result;
            })
    }


    AssignAlert(result, accProfilephone) {
        return swal({
            title: 'Load Cancelled',
            showConfirmButton: true,
            allowOutsideClick: false,
            confirmButtonText: 'OK',
            confirmButtonClass: "btn btn-warning",
            //   buttonsStyling: false
        })
            .then((result) => {
                return result;
            })
    }

    BidStatus(result, accProfilephone) {

        if (result == "Bid Closed") {

            return swal({
                title: 'Bid Expired',
                showConfirmButton: true,
                allowOutsideClick: false,
                confirmButtonText: 'OK',
                confirmButtonClass: "btn btn-warning",
                //   buttonsStyling: false
            })
                .then((result) => {
                    return result;
                })
        }
        if (result == "Already Awarded") {
            return swal({
                title: 'This load has been Cancelled.',
                showConfirmButton: true,
                allowOutsideClick: false,
                confirmButtonText: 'OK',
                confirmButtonClass: "btn btn-warning",
                //   buttonsStyling: false
            })
                .then((result) => {
                    return result;
                })
        }
        if (result == "Bid Submitted") {
            return swal({
                title: 'Bid already submitted, please contact ' + accProfilephone + ' to cancel your bid.',
                showConfirmButton: true,
                allowOutsideClick: false,
                confirmButtonText: 'OK',
                confirmButtonClass: "btn btn-warning",
                //   buttonsStyling: false
            })
                .then((result) => {
                    return result;
                })
        }
        if (result == "Cancelled") {
            return swal({
                title: 'Load Cancelled',
                showConfirmButton: true,
                allowOutsideClick: false,
                confirmButtonText: 'OK',
                confirmButtonClass: "btn btn-warning",
                //   buttonsStyling: false
            })
                .then((result) => {
                    return result;
                })
        }
        if (result == "Cancelled") {
            return swal({
                title: 'Load Cancelled',
                showConfirmButton: true,
                allowOutsideClick: false,
                confirmButtonText: 'OK',
                confirmButtonClass: "btn btn-warning",
                //   buttonsStyling: false
            })
                .then((result) => {
                    return result;
                })
        }

    }
    BidAcceptRejectStatus(result) {

        if (result == "Already Confirmed") {
            return swal({
                title: 'Load already Confirmed',
                showConfirmButton: true,
                allowOutsideClick: false,
                confirmButtonText: 'OK',
                confirmButtonClass: "btn btn-warning",
                //   buttonsStyling: false
            })
                .then((result) => {
                    return result;
                })
        }

        if (result == "Already Rejected") {
            return swal({
                title: 'This Load is Rejected.',
                showConfirmButton: true,
                allowOutsideClick: false,
                confirmButtonText: 'OK',
                confirmButtonClass: "btn btn-warning",
                //   buttonsStyling: false
            })
                .then((result) => {
                    return result;
                })
        }
    }

    BidSaveValues() {
        return swal({
            title: 'Actual Quote Value must be greater than Alternate Quote Value',
            showConfirmButton: true,
            allowOutsideClick: false,
            confirmButtonText: 'OK',
            confirmButtonClass: "btn btn-warning",
            //   buttonsStyling: false
        })
            .then((result) => {
                return result;
            })

    }

    usdot(data: any): any {
        return swal({
            title: 'This USDOT Number Already Registered in the System.',
            showConfirmButton: true,
            allowOutsideClick: false,
            confirmButtonText: 'OK',
            confirmButtonClass: "btn btn-warning",

        })
            .then((result) => {
                return result;
            })

    }

    customerName(): any {
        return swal({
            title: 'This Customer is already registered. If you would like to add new Location to the existing customer, select customer from drop-down list. Else, add unique name to the customer.',
            showConfirmButton: true,
            allowOutsideClick: false,
            confirmButtonText: 'OK',
            confirmButtonClass: "btn btn-warning",

        })
            .then((result) => {
                return result;
            })

    }


    email(data: any): any {
        return swal({
            title: 'This Email ID Already Registered in the System.',
            showConfirmButton: true,
            allowOutsideClick: false,
            confirmButtonText: 'OK',
            confirmButtonClass: "btn btn-warning",

        })
            .then((result) => {
                return result;
            })

    }


    commonauthority(data: any): any {
        return swal({
            title: 'This Carrier has Common Authority Status CONDITIONAL So We Can Not Add.',
            showConfirmButton: true,
            allowOutsideClick: false,
            confirmButtonText: 'OK',
            confirmButtonClass: "btn btn-warning",

        })
            .then((result) => {
                return result;
            })

    }


    sendReconfirmation(): any {

        return swal({

            title: "<span style='font-size:17px;'>",
            html: "<span style='font-size:15px;'>Do you want to send a revised confirmation to the carrier? ",

            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: "mat-flat-button mat-primary carrierDeactivationBtn button1",
            cancelButtonClass: "mat-flat-button mat-accent button1",
            buttonsStyling: false,
            allowOutsideClick: false
        })

            .then((result) => {
                return result;
            })
    }

    showRemoteCheckinFirst(): any {

        return swal({
            title: 'Please Checkin First',
            showConfirmButton: true,
            allowOutsideClick: false,
            confirmButtonText: 'OK',
            confirmButtonClass: "btn btn-warning",

        })
            .then((result) => {
                return result;
            })
    }


    showPickdropChange(): any {

        return swal({
            title: 'This Load Route has been changed',
            showConfirmButton: true,
            allowOutsideClick: false,
            confirmButtonText: 'OK',
            confirmButtonClass: "btn btn-warning",

        })
            .then((result) => {
                return result;
            })
    }



    deletedocumentalert() {
        return swal({

            title: "<span style='font-size:17px;'>Confirm Delete?",
            html: "<span style='font-size:15px;'>This action will permanently delete carrier paperwork for the selected stop.",

            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: "mat-flat-button mat-primary carrierDeactivationBtn button1",
            cancelButtonClass: "mat-flat-button mat-accent button1",
            buttonsStyling: false,
            allowOutsideClick: false
        })

            .then((result) => {
                return result;
            })
    }

  acceptAlert(data: any): any {
        return swal({
            title: 'Are you sure you want to Accept this EDI load?',
            text: '',  //this imaginary file!,
            //type: 'warning',
            showCancelButton: true,
            allowOutsideClick: false,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: "mat-flat-button mat-primary margin",
            cancelButtonClass: "mat-flat-button mat-accent",
            buttonsStyling: false
        })
            .then((result) => {
                return result;
            })
    }
    Changecarrier(): any {

        return swal({

            title: "<span style='font-size:17px;'>",
            html: "<span style='font-size:15px;'>Do you want to Change Carrier ? ",

            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: "mat-flat-button mat-primary carrierDeactivationBtn button1",
            cancelButtonClass: "mat-flat-button mat-accent button1",
            buttonsStyling: false,
            allowOutsideClick: false
        })

            .then((result) => {
                return result;
            })
    }

declineAlert(data: any): any {
        return swal({
            title: 'Are you sure you want to Decline this EDI load?',
           // text: 'This will send 990.',  //this imaginary file!,
            //type: 'warning',
            showCancelButton: true,
            allowOutsideClick: false,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: "mat-flat-button mat-primary margin",
            cancelButtonClass: "mat-flat-button mat-accent",
            buttonsStyling: false
        })
            .then((result) => {
                return result;
            })
    }
    AlreadyDelivered(){
            return swal({
                title: 'Load Is Delivered',
                showConfirmButton: true,
                allowOutsideClick: false,
                confirmButtonText: 'OK',
                confirmButtonClass: "btn btn-warning",
                //   buttonsStyling: false
            })
                .then((result) => {
                    return result;
                })
        }
}