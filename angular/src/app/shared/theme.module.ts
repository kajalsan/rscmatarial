import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThemePickerComponent } from './theme-picker/theme-picker.component';
import { MatMenuModule, MatButtonModule, MatGridListModule, MatIconModule } from '@angular/material';

@NgModule({
  declarations: [ThemePickerComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatGridListModule,
    MatIconModule,
    MatMenuModule
    
  ],
  exports: [
    ThemePickerComponent,
    MatButtonModule
  ]
})
export class ThemeModule { }
