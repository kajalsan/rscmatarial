import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'app-customer-dialog',
    templateUrl: './customerDialog.component.html',
})

export class CustomerDialogComponent {
    constructor(
        private _myDialogRef: MatDialogRef<CustomerDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private data: string
    ) {}

    // onCloseConfirm() {
    //     this._myDialogRef.close('Confirm');
    //   }

    onCloseCancel() {
    this._myDialogRef.close('Close');
    //console.log(this.data);
    }
}
