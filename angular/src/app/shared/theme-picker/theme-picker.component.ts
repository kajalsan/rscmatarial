import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { StyleManagerService } from '../style-manager.service';

@Component({
  selector: 'app-theme-picker',
  templateUrl: './theme-picker.component.html',
  styleUrls: ['./theme-picker.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ThemePickerComponent implements OnInit {

  themes: CustomTheme[] = [
    {
      primary: '#ed1c24',
      accent: '#cccccc',
      name: 'default',
      isDark: false,
      logo: '828_logo.png',
      isDefault: true,
    },
    // {
    //   primary: '#9c27b0',
    //   accent: '#ed1c24',
    //   name: 'toyota',
    //   isDark: false,
    //   logo: 'toyota_logo.png',
    // },
    // {
    //   primary: '#ed1c24',
    //   accent: '#607D8B',
    //   name: 'denso',
    //   isDark: true,
    //   logo: 'tenant_logo.png',
    // },
    // {
    //   primary: '#33691e',
    //   accent: '#4CAF50',
    //   name: 'subaru',
    //   isDark: true,
    //   logo: 'tenant_logo.png',
    // },
    {
      primary: '#DD5F43',
      accent: '#999999',
      name: 'theme2',
      isDark: false,
      logo: 'toyota_logo.png',
    },
    {
      primary: '#2EC4B6',
      accent: '#607D8B',
      name: 'theme3',
      isDark: true,
      logo: 'tenant_logo.png',
    },
    {
      primary: '#4f6367',
      accent: '#4CAF50',
      name: 'theme4',
      isDark: true,
      logo: 'tenant_logo.png',
    },
    {
      primary: '#171d1c',
      accent: '#4CAF50',
      name: 'theme5',
      isDark: true,
      logo: 'tenant_logo.png',
    },
  ];

  constructor(
    public styleManager: StyleManagerService,
  ) { }

  ngOnInit() {
    if(localStorage.length > 0 && (localStorage.getItem('styleName') !== '' || localStorage.getItem('styleName') !== undefined))
      this.installTheme(localStorage.getItem('styleName'));
    else
      this.installTheme('default');
  }

  installTheme(themeName: string) {
    const theme = this.themes.find(currentTheme => currentTheme.name === themeName);
    if (!theme) {
      return;
    }

    $('body').removeClass(localStorage.getItem('styleName'));

    // if (theme.isDefault) {
    //   this.styleManager.removeStyle('theme');
    // } else {
    //   //this.styleManager.setStyle('theme', `/assets/themes/${theme.name}.css`);
    //   this.styleManager.setStyle('theme', `/assets/themes/${theme.name}/default.css`);
      
    // }

    $('body').addClass(theme.name);
    localStorage.setItem('styleName', theme.name);

    // let urlName = document.getElementById('themeAsset') as HTMLAnchorElement;
    // urlName.href = `/assets/themes/${theme.name}/default.scss`;
  }

}



export interface CustomTheme {
  name: string;
  accent: string;
  primary: string;
  isDark?: boolean;
  isDefault?: boolean;
  logo?: string;
}
