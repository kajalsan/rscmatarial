import { Injectable } from '@angular/core';

@Injectable()
export class DateFormatUTCService {
  private _dateTimeMin;
  private _dateTimeHrs;

  constructor() { }

  convertBidTimeToUTC(_dateTime, _timer){
    if(_dateTime._a[4] + _timer >= 60 ) {
      this._dateTimeMin = _dateTime._a[4] + _timer - 60;
      this._dateTimeHrs =  _dateTime._a[3] + 1;
    } else {
      this._dateTimeMin = _dateTime._a[4] + _timer;
      this._dateTimeHrs =  _dateTime._a[3];
    }
    return Date.UTC(_dateTime._a[0], _dateTime._a[1], _dateTime._a[2], this._dateTimeHrs, this._dateTimeMin, _dateTime._a[5])
  }
  
  convertLocalTimeToUTC(_dateTime){
    return Date.UTC(_dateTime._a[0], _dateTime._a[1], _dateTime._a[2], _dateTime._a[3], _dateTime._a[4], _dateTime._a[5])
  }
}