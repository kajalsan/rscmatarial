import { NgModule } from "@angular/core";
import { ConfirmEqualValidatorDirective } from "@app/shared/confirm-equal-validator.directive";
import { RestrictInputDirective } from "@app/shared/restrict-input.directive";
import { CountDown } from "@app/shared/countdown.component";
import { PhoneMaskDirective } from "@app/shared/phone-mask.directive";
import { TimeMaskDirective } from "@app/shared/time-mask.directive"
import { MCNumberDirective } from "@app/shared/mcnumber-mask.directive";
import { TaxIDMaskDirective } from "@app/shared/taxidein-mask.directive";
import {TwoDigitDecimaNumberDirective} from "@app/shared/two-digit-decima-number.directive";
import { NumberOnlyDirective } from "@app/shared/NumberOnlyDirective.component";
import { AlphabetOnlyDirective } from "@app/shared/alphabet.only.directive";
import { AlphabetNumberDirective } from "@app/shared/alphabet.number.directive";
import {SpecialCharacterDirective} from "@app/shared/specialIsAlphaNumeric";


@NgModule({
    declarations: [ConfirmEqualValidatorDirective, CountDown,NumberOnlyDirective,
        RestrictInputDirective, PhoneMaskDirective,
        TimeMaskDirective, MCNumberDirective, 
        AlphabetOnlyDirective, AlphabetNumberDirective,
        TaxIDMaskDirective,TwoDigitDecimaNumberDirective,SpecialCharacterDirective],
    exports: [ConfirmEqualValidatorDirective, CountDown, RestrictInputDirective,NumberOnlyDirective
        , PhoneMaskDirective,
         TimeMaskDirective, MCNumberDirective, 
         AlphabetOnlyDirective, AlphabetNumberDirective,
        TaxIDMaskDirective,TwoDigitDecimaNumberDirective,SpecialCharacterDirective]
})

export class MyCommonModule { }
