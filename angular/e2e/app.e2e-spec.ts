import { exampledemoTemplatePage } from './app.po';

describe('exampledemo App', function() {
  let page: exampledemoTemplatePage;

  beforeEach(() => {
    page = new exampledemoTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
